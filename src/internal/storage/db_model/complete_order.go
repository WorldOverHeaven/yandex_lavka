package db_model

type CompleteOrder struct {
	CourierID int64
	OrderID   int64
}
