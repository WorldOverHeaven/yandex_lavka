package handler

import (
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"net/http"
	"net/http/httptest"
	"testing"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
)

func TestHandler_CompleteOrder(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodPost, "/orders/complete", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/orders/complete")

				fields.ctrl.EXPECT().CompleteOrder(gomock.Any(), model.CompleteOrderRequestDto{}).Return(
					[]model.OrderDto{},
					model.HandlerError{},
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}
			if err := h.CompleteOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("CompleteOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHandler_CreateOrder(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodPost, "/orders", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/orders")

				fields.ctrl.EXPECT().CreateOrder(gomock.Any(), model.CreateOrderRequest{}).Return(
					[]model.OrderDto{},
					model.HandlerError{},
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}
			if err := h.CreateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("CreateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHandler_GetOrder(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodPost, "/orders/:order_id", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/orders/:order_id")
				args.ctx.SetParamNames("order_id")
				args.ctx.SetParamValues("1")

				fields.ctrl.EXPECT().GetOrder(gomock.Any(), int64(1)).Return(
					model.OrderDto{},
					model.HandlerError{},
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}
			if err := h.GetOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GetOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHandler_GetOrders(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodGet, "/orders", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/orders")

				fields.ctrl.EXPECT().GetOrders(gomock.Any(), int32(1), int32(0)).Return(
					[]model.OrderDto{},
					model.HandlerError{},
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}
			if err := h.GetOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GetOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
