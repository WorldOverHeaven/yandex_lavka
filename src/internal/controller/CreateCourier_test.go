package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_CreateCourier(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx     context.Context
		request model.CreateCourierRequest
	}
	tests := []struct {
		name    string
		args    args
		want    model.CreateCouriersResponse
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx: context.Background(),
				request: model.CreateCourierRequest{
					Couriers: []model.CreateCourierDto{{
						CourierType:  "FOOT",
						Regions:      []int32{1},
						WorkingHours: []string{"11:00-12:00"},
					},
					},
				},
			},
			want: model.CreateCouriersResponse{Couriers: []model.CourierDto{{
				CourierId:    1,
				CourierType:  "FOOT",
				Regions:      []int32{1},
				WorkingHours: []string{"11:00-12:00"},
			},
			},
			},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().CreateCourier(gomock.Any(),
					[]db_model.Courier{{
						ID:          0,
						CourierType: "FOOT",
						Region:      []int32{1},
						WorkingHour: []string{"11:00-12:00"},
					}}).Return(
					[]db_model.Courier{{
						ID:          1,
						CourierType: "FOOT",
						Region:      []int32{1},
						WorkingHour: []string{"11:00-12:00"},
					}},
					nil,
				)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx: context.Background(),
				request: model.CreateCourierRequest{
					Couriers: []model.CreateCourierDto{{
						CourierType:  "Test",
						Regions:      []int32{1},
						WorkingHours: []string{"11:00-12:00"},
					},
					},
				},
			},
			want: model.CreateCouriersResponse{Couriers: nil},
			want1: model.HandlerError{
				Err:  errors.New("error in courierType data"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_3",
			args: args{
				ctx: context.Background(),
				request: model.CreateCourierRequest{
					Couriers: []model.CreateCourierDto{{
						CourierType:  "FOOT",
						Regions:      []int32{1},
						WorkingHours: []string{"test"},
					},
					},
				},
			},
			want: model.CreateCouriersResponse{Couriers: nil},
			want1: model.HandlerError{
				Err:  errors.New("error in hours"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_4",
			args: args{
				ctx: context.Background(),
				request: model.CreateCourierRequest{
					Couriers: []model.CreateCourierDto{{
						CourierType:  "FOOT",
						Regions:      []int32{-1},
						WorkingHours: []string{"11:00-12:00"},
					},
					},
				},
			},
			want: model.CreateCouriersResponse{Couriers: nil},
			want1: model.HandlerError{
				Err:  errors.New("error in region data"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_5",
			args: args{
				ctx: context.Background(),
				request: model.CreateCourierRequest{
					Couriers: []model.CreateCourierDto{{
						CourierType:  "FOOT",
						Regions:      []int32{1},
						WorkingHours: []string{"11:00-12:00"},
					},
					},
				},
			},
			want: model.CreateCouriersResponse{Couriers: nil},
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().CreateCourier(gomock.Any(),
					[]db_model.Courier{{
						ID:          0,
						CourierType: "FOOT",
						Region:      []int32{1},
						WorkingHour: []string{"11:00-12:00"},
					}}).Return(
					nil,
					errors.New("err"),
				)
			},
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}

			got, got1 := c.CreateCourier(tt.args.ctx, tt.args.request)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateCourier() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("CreateCourier() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
