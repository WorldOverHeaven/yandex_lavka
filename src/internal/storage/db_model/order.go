package db_model

import "time"

type Order struct {
	ID            int64
	Weight        float32
	Region        int32
	Cost          int32
	CompletedTime time.Time
	DeliveryHour  []string
}
