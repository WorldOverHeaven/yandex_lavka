/*
 * Yandex Lavka
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package model

type GetCourierMetaInfoResponse struct {
	CourierId int64 `json:"courier_id"`

	CourierType string `json:"courier_type"`

	Regions []int32 `json:"regions"`

	WorkingHours []string `json:"working_hours"`

	Rating int32 `json:"rating,omitempty"`

	Earnings int32 `json:"earnings,omitempty"`
}
