package controller

import (
	"context"
	"time"
	"yandex_lavka/internal/model"
)

type Controller struct {
	storage Storage
}

func NewController(storage Storage) *Controller {
	return &Controller{storage: storage}
}

// OrdersAssign e.POST("/orders/assign", OrdersAssign)
func (c *Controller) OrdersAssign(ctx context.Context, date time.Time) ([]model.OrderAssignResponse, model.HandlerError) {
	return nil, model.HandlerError{}
}

// CouriersAssignments e.GET("/couriers/assignments", CouriersAssignments)
func (c *Controller) CouriersAssignments(ctx context.Context, date time.Time, courierId int64) (model.OrderAssignResponse, model.HandlerError) {
	return model.OrderAssignResponse{}, model.HandlerError{}
}

func (c *Controller) Close() {
}
