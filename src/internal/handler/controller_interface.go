package handler

import (
	"context"
	"time"
	"yandex_lavka/internal/model"
)

type Controller interface {
	// GetOrders e.GET("/orders", GetOrders)
	GetOrders(ctx context.Context, limit, offset int32) ([]model.OrderDto, model.HandlerError)

	// CreateOrder e.POST("/orders", CreateOrder)
	CreateOrder(ctx context.Context, request model.CreateOrderRequest) ([]model.OrderDto, model.HandlerError)

	// CompleteOrder e.POST("/orders/complete", CompleteOrder)
	CompleteOrder(ctx context.Context, dto model.CompleteOrderRequestDto) ([]model.OrderDto, model.HandlerError)

	// OrdersAssign e.POST("/orders/assign", OrdersAssign)
	OrdersAssign(ctx context.Context, date time.Time) ([]model.OrderAssignResponse, model.HandlerError)

	// GetOrder e.GET("/orders/:order_id", GetOrder)
	GetOrder(ctx context.Context, id int64) (model.OrderDto, model.HandlerError)

	// CouriersAssignments e.GET("/couriers/assignments", CouriersAssignments)
	CouriersAssignments(ctx context.Context, date time.Time, courierId int64) (model.OrderAssignResponse, model.HandlerError)

	// GetCouriers e.GET("/couriers", GetCouriers)
	GetCouriers(ctx context.Context, limit, offset int32) (model.GetCouriersResponse, model.HandlerError)

	// CreateCourier e.POST("/couriers", CreateCourier)
	CreateCourier(ctx context.Context, request model.CreateCourierRequest) (model.CreateCouriersResponse, model.HandlerError)

	// GetCourierById e.GET("/couriers/:courier_id", GetCourierById)
	GetCourierById(ctx context.Context, id int64) (model.CourierDto, model.HandlerError)

	// GetCourierMetaInfo e.GET("/couriers/meta-info/:courier_id", GetCourierMetaInfo)
	GetCourierMetaInfo(ctx context.Context, courierId int64, startDate, endDate time.Time) (model.GetCourierMetaInfoResponse, model.HandlerError)

	Close()
}
