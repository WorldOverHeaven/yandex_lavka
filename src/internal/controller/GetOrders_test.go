package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"time"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_GetOrders(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx    context.Context
		limit  int32
		offset int32
	}
	tests := []struct {
		name    string
		args    args
		want    []model.OrderDto
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx:    context.Background(),
				limit:  1,
				offset: 0,
			},
			want: []model.OrderDto{{
				OrderId:       1,
				Weight:        2,
				Regions:       3,
				DeliveryHours: []string{"10:00-12:00"},
				Cost:          100,
				CompletedTime: time.Time{},
			}},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetOrders(gomock.Any(), args.limit, args.offset).Return(
					[]db_model.Order{{
						ID:            1,
						Weight:        2,
						Region:        3,
						Cost:          100,
						CompletedTime: time.Time{},
						DeliveryHour:  []string{"10:00-12:00"},
					},
					},
					nil,
				)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx:    context.Background(),
				limit:  1,
				offset: 0,
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetOrders(gomock.Any(), args.limit, args.offset).Return(
					nil,
					errors.New("err"),
				)
			},
		},
		{
			name: "test_3",
			args: args{
				ctx:    context.Background(),
				limit:  -1,
				offset: -1,
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("error in limit offset"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}
			got, got1 := c.GetOrders(tt.args.ctx, tt.args.limit, tt.args.offset)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetOrders() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetOrders() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
