package storage

import (
	"context"
	"fmt"
	"github.com/go-pg/pg/v10"
	"time"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

type Storage struct {
	db *pg.DB
}

func NewStorage(db *pg.DB) *Storage {
	return &Storage{db: db}
}

func (s *Storage) Close() {
	err := s.db.Close()
	if err != nil {
		return
	}
	panic(err)
}

func (s *Storage) Setup() {
}

func (s *Storage) CreateOrders(ctx context.Context, orders []db_model.Order) ([]db_model.Order, error) {
	_, err := s.db.WithContext(ctx).Model(&orders).Insert()

	if err != nil {
		return nil, err
	}

	return orders, nil
}

func (s *Storage) GetOrders(ctx context.Context, limit, offset int32) ([]db_model.Order, error) {
	var orders []db_model.Order
	err := s.db.WithContext(ctx).Model(&orders).Limit(int(limit)).Offset(int(offset)).Select()

	if err != nil {
		return nil, err
	}

	return orders, nil
}

func (s *Storage) GetOrder(ctx context.Context, id int64) (db_model.Order, error) {
	order := db_model.Order{
		ID: id,
	}

	err := s.db.WithContext(ctx).Model(&order).WherePK().Select()

	if err != nil {
		return order, err
	}

	return order, nil
}

func (s *Storage) CreateCourier(ctx context.Context, couriers []db_model.Courier) ([]db_model.Courier, error) {
	_, err := s.db.WithContext(ctx).Model(&couriers).Insert()

	if err != nil {
		return nil, err
	}

	return couriers, nil
}

func (s *Storage) GetCouriers(ctx context.Context, limit, offset int32) ([]db_model.Courier, error) {
	var couriers []db_model.Courier
	err := s.db.WithContext(ctx).Model(&couriers).Limit(int(limit)).Offset(int(offset)).Select()

	if err != nil {
		return nil, err
	}

	return couriers, nil
}

func (s *Storage) GetCourierById(ctx context.Context, id int64) (db_model.Courier, error) {
	courier := db_model.Courier{
		ID: id,
	}

	err := s.db.WithContext(ctx).Model(&courier).WherePK().Select()

	if err != nil {
		return courier, err
	}

	return courier, nil
}

func (s *Storage) CompleteOrder(ctx context.Context, completeOrders []model.CompleteOrder) ([]db_model.Order, error) {
	orders := make([]db_model.Order, 0, len(completeOrders))

	for _, completeOrder := range completeOrders {
		order, err := s.GetOrder(ctx, completeOrder.OrderId)
		if err != nil {
			return nil, fmt.Errorf("no order: %w", err)
		}

		_, err = s.GetCourierById(ctx, completeOrder.CourierId)
		if err != nil {
			return nil, fmt.Errorf("no courier: %w", err)
		}

		dbCompleteOrder := db_model.CompleteOrder{
			CourierID: completeOrder.CourierId,
			OrderID:   completeOrder.OrderId,
		}

		ok, err := s.db.WithContext(ctx).
			Model(&dbCompleteOrder).
			Where("courier_id = ?", completeOrder.CourierId).
			Where("order_id = ?", completeOrder.OrderId).
			Exists()

		if err != nil {
			return nil, fmt.Errorf("can't find completeOrder: %w", err)
		}

		if !ok {
			_, err = s.db.WithContext(ctx).Model(&dbCompleteOrder).Insert()
			if err != nil {
				return nil, fmt.Errorf("can't insert completeOrder: %w", err)
			}
		}

		if completeOrder.CompleteTime != order.CompletedTime {
			order.CompletedTime = completeOrder.CompleteTime
			_, err = s.db.WithContext(ctx).Model(&order).WherePK().Update()
			if err != nil {
				return nil, fmt.Errorf("can't update completeOrder: %w", err)
			}
		}

		orders = append(orders, order)
	}

	return orders, nil
}

func (s *Storage) GetCourierMetaInfo(ctx context.Context, courierId int64, startDate, endDate time.Time) ([]db_model.Order, error) {
	var orders []db_model.Order

	var completeOrders []db_model.CompleteOrder

	err := s.db.WithContext(ctx).Model(&completeOrders).Where("courier_id = ?", courierId).Select()
	if err != nil {
		return nil, fmt.Errorf("can't select orders_id %w", err)
	}

	if len(completeOrders) == 0 {
		return []db_model.Order{}, nil
	}

	ordersId := make([]int64, 0, len(completeOrders))

	for _, val := range completeOrders {
		ordersId = append(ordersId, val.OrderID)
	}

	err = s.db.WithContext(ctx).
		Model(&orders).
		Where("id IN (?)", pg.In(ordersId)).
		Where("completed_time >= ?", startDate).
		Where("completed_time < ?", endDate).
		Select()
	if err != nil {
		return nil, fmt.Errorf("can't select orders %w", err)
	}

	return orders, nil
}
