package controller

import (
	"context"
	"time"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

// CreateOrder e.POST("/orders", CreateOrder)
func (c *Controller) CreateOrder(ctx context.Context, request model.CreateOrderRequest) ([]model.OrderDto, model.HandlerError) {
	err := validateCreateOrder(request)
	if err != nil {
		return nil, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	orders := make([]db_model.Order, len(request.Orders))
	for key, val := range request.Orders {
		orders[key] = db_model.Order{
			Weight:        val.Weight,
			Region:        val.Regions,
			Cost:          val.Cost,
			CompletedTime: time.Time{},
			DeliveryHour:  val.DeliveryHours,
		}
	}

	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	orders, err = c.storage.CreateOrders(ctx, orders)
	if err != nil {
		return nil, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	returnOrders := make([]model.OrderDto, len(orders))
	for key, val := range orders {
		returnOrders[key] = model.OrderDto{
			OrderId:       val.ID,
			Weight:        val.Weight,
			Regions:       val.Region,
			DeliveryHours: val.DeliveryHour,
			Cost:          val.Cost,
			CompletedTime: val.CompletedTime,
		}
	}

	return returnOrders, model.HandlerError{}
}

func validateCreateOrder(request model.CreateOrderRequest) error {
	for _, val := range request.Orders {
		err := ValidateWeight(val.Weight)
		if err != nil {
			return err
		}

		err = ValidateRegion(val.Regions)
		if err != nil {
			return err
		}

		for _, val := range val.DeliveryHours {
			err := ValidateHours(val)
			if err != nil {
				return err
			}
		}

		err = ValidateCost(val.Cost)
		if err != nil {
			return err
		}
	}
	return nil
}
