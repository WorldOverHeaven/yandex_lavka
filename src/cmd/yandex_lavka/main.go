package main

import (
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"log"
	"time"
	"yandex_lavka/internal/controller"
	"yandex_lavka/internal/handler"
	"yandex_lavka/internal/storage"
	"yandex_lavka/internal/storage/db_model"
)

func main() {
	e := setupServer()
	e.Logger.Fatal(e.Start(":8080"))
}

func setupServer() *echo.Echo {
	e := echo.New()

	e.Use(Logger)

	e.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(10)))

	conn := pg.Connect(&pg.Options{
		User:     "postgres",
		Password: "password",
		Addr:     "db:5432",
		Database: "postgres",
	})

	err := createSchema(conn)

	if err != nil {
		panic(err)
	}

	db := storage.NewStorage(conn)

	ctrl := controller.NewController(db)

	h := handler.NewHandler(ctrl)

	handler.SetupRoutes(e, h)

	return e
}

func Logger(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		start := time.Now()

		if err := next(c); err != nil {
			c.Error(err)
		}

		log.Printf(
			"%s %s",
			c.Path(),
			time.Since(start),
		)
		return nil
	}
}

func createSchema(db *pg.DB) error {
	models := []interface{}{
		(*db_model.Courier)(nil),
		(*db_model.Order)(nil),
		(*db_model.CompleteOrder)(nil),
	}

	for _, m := range models {
		err := db.Model(m).CreateTable(&orm.CreateTableOptions{
			Temp:        false,
			IfNotExists: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
