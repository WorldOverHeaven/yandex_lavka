package controller

import (
	"context"
	"errors"
	"yandex_lavka/internal/model"
)

// GetCouriers e.GET("/couriers", GetCouriers)
func (c *Controller) GetCouriers(ctx context.Context, limit, offset int32) (model.GetCouriersResponse, model.HandlerError) {
	if limit < 0 || offset < 0 {
		return model.GetCouriersResponse{}, model.HandlerError{
			Err:  errors.New("error in limit offset"),
			Type: 500,
		}
	}

	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	couriers, err := c.storage.GetCouriers(ctx, limit, offset)
	if err != nil {
		return model.GetCouriersResponse{}, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	couriersReturn := model.GetCouriersResponse{
		Couriers: make([]model.CourierDto, limit),
		Limit:    limit,
		Offset:   offset,
	}

	for key, val := range couriers {
		couriersReturn.Couriers[key] = model.CourierDto{
			CourierId:    val.ID,
			CourierType:  val.CourierType,
			Regions:      val.Region,
			WorkingHours: val.WorkingHour,
		}
	}

	return couriersReturn, model.HandlerError{}
}
