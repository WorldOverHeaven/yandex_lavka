package controller

import (
	"context"
	"github.com/pkg/errors"
	"yandex_lavka/internal/model"
)

// GetCourierById e.GET("/couriers/:courier_id", GetCourierById)
func (c *Controller) GetCourierById(ctx context.Context, id int64) (model.CourierDto, model.HandlerError) {
	if id <= 0 {
		return model.CourierDto{}, model.HandlerError{
			Err:  errors.New("incorrect id"),
			Type: 500,
		}
	}

	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	order, err := c.storage.GetCourierById(ctx, id)
	if err != nil {
		return model.CourierDto{}, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	orderReturn := model.CourierDto{
		CourierId:    order.ID,
		CourierType:  order.CourierType,
		Regions:      order.Region,
		WorkingHours: order.WorkingHour,
	}

	return orderReturn, model.HandlerError{}
}
