package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_GetCouriers(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx    context.Context
		limit  int32
		offset int32
	}
	tests := []struct {
		name    string
		args    args
		want    model.GetCouriersResponse
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx:    context.Background(),
				limit:  1,
				offset: 0,
			},
			want: model.GetCouriersResponse{
				Couriers: []model.CourierDto{{
					CourierId:    1,
					CourierType:  "FOOT",
					Regions:      []int32{1},
					WorkingHours: []string{"10:00-12:00"},
				},
				},
				Limit:  1,
				Offset: 0,
			},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCouriers(gomock.Any(), args.limit, args.offset).Return(
					[]db_model.Courier{{
						ID:          1,
						CourierType: "FOOT",
						Region:      []int32{1},
						WorkingHour: []string{"10:00-12:00"},
					}},
					nil,
				)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx:    context.Background(),
				limit:  -1,
				offset: -1,
			},
			want: model.GetCouriersResponse{},
			want1: model.HandlerError{
				Err:  errors.New("error in limit offset"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_3",
			args: args{
				ctx:    context.Background(),
				limit:  1,
				offset: 0,
			},
			want: model.GetCouriersResponse{
				Couriers: nil,
				Limit:    0,
				Offset:   0,
			},
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCouriers(gomock.Any(), args.limit, args.offset).Return(
					nil,
					errors.New("err"),
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}
			got, got1 := c.GetCouriers(tt.args.ctx, tt.args.limit, tt.args.offset)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCouriers() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetCouriers() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
