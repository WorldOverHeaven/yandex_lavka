package controller

import (
	"context"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

// CreateCourier e.POST("/couriers", CreateCourier)
func (c *Controller) CreateCourier(ctx context.Context, request model.CreateCourierRequest) (model.CreateCouriersResponse, model.HandlerError) {
	err := validateCreateCourier(request)
	if err != nil {
		return model.CreateCouriersResponse{}, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	couriers := make([]db_model.Courier, len(request.Couriers))
	for key, val := range request.Couriers {
		couriers[key] = db_model.Courier{
			CourierType: val.CourierType,
			Region:      val.Regions,
			WorkingHour: val.WorkingHours,
		}
	}

	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	couriers, err = c.storage.CreateCourier(ctx, couriers)
	if err != nil {
		return model.CreateCouriersResponse{}, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	couriersReturn := model.CreateCouriersResponse{Couriers: make([]model.CourierDto, len(couriers))}
	for key, val := range couriers {
		couriersReturn.Couriers[key] = model.CourierDto{
			CourierId:    val.ID,
			CourierType:  val.CourierType,
			Regions:      val.Region,
			WorkingHours: val.WorkingHour,
		}
	}

	return couriersReturn, model.HandlerError{}
}

func validateCreateCourier(request model.CreateCourierRequest) error {
	for _, val := range request.Couriers {
		for _, val := range val.Regions {
			err := ValidateRegion(val)
			if err != nil {
				return err
			}
		}
		for _, val := range val.WorkingHours {
			err := ValidateHours(val)
			if err != nil {
				return err
			}
		}
		err := ValidateCourierType(val.CourierType)
		if err != nil {
			return err
		}
	}
	return nil
}
