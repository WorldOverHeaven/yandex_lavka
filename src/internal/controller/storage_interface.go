package controller

import (
	"context"
	"time"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

type Storage interface {
	CreateOrders(ctx context.Context, orders []db_model.Order) ([]db_model.Order, error)
	GetOrders(ctx context.Context, limit, offset int32) ([]db_model.Order, error)
	GetOrder(ctx context.Context, id int64) (db_model.Order, error)
	CreateCourier(ctx context.Context, couriers []db_model.Courier) ([]db_model.Courier, error)
	GetCouriers(ctx context.Context, limit, offset int32) ([]db_model.Courier, error)
	GetCourierById(ctx context.Context, id int64) (db_model.Courier, error)
	CompleteOrder(ctx context.Context, completeOrders []model.CompleteOrder) ([]db_model.Order, error)
	GetCourierMetaInfo(ctx context.Context, courierId int64, startDate, endDate time.Time) ([]db_model.Order, error)
}
