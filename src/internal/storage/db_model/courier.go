package db_model

type Courier struct {
	ID          int64
	CourierType string
	Region      []int32
	WorkingHour []string
}
