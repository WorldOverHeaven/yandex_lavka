package controller

import (
	"context"
	"errors"
	"yandex_lavka/internal/model"
)

// GetOrder e.GET("/orders/:order_id", GetOrder)
func (c *Controller) GetOrder(ctx context.Context, id int64) (model.OrderDto, model.HandlerError) {
	if id <= 0 {
		return model.OrderDto{}, model.HandlerError{
			Err:  errors.New("incorrect id"),
			Type: 500,
		}
	}
	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	order, err := c.storage.GetOrder(ctx, id)
	if err != nil {
		return model.OrderDto{}, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	returnOrder := model.OrderDto{
		OrderId:       order.ID,
		Weight:        order.Weight,
		Regions:       order.Region,
		DeliveryHours: order.DeliveryHour,
		Cost:          order.Cost,
		CompletedTime: order.CompletedTime,
	}

	return returnOrder, model.HandlerError{}
}
