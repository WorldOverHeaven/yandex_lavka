package controller

import (
	"errors"
	"fmt"
	"regexp"
	"yandex_lavka/internal/consts"
)

func ValidateHours(workingHours string) error {
	ok, err := regexp.MatchString(`^([01][0-9]|2[0-3]):([0-5][0-9])-([01][0-9]|2[0-3]):([0-5][0-9])$`, workingHours)
	if err != nil {
		return fmt.Errorf("error in regexp %w", err)
	}
	if !ok {
		return errors.New("error in hours")
	}
	return nil
}

func ValidateRegion(region int32) error {
	if region < 0 {
		return errors.New("error in region data")
	}
	return nil
}

func ValidateCourierType(courierType string) error {
	switch courierType {
	case consts.FOOT:
		return nil
	case consts.BIKE:
		return nil
	case consts.AUTO:
		return nil
	default:
		return errors.New("error in courierType data")
	}
}

func ValidateWeight(weight float32) error {
	if weight <= 0 {
		return errors.New("error in weight data")
	}
	return nil
}

func ValidateCost(cost int32) error {
	if cost <= 0 {
		return errors.New("error in cost data")
	}
	return nil
}
