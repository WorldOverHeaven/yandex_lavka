package handler

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"net/http"
	"net/http/httptest"
	"testing"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
)

func TestHandler_GetCourierById(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodGet, "/couriers/1", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/couriers/:courier_id")
				args.ctx.SetParamNames("courier_id")
				args.ctx.SetParamValues("1")

				fields.ctrl.EXPECT().GetCourierById(gomock.Any(), int64(1)).Return(
					model.CourierDto{},
					model.HandlerError{},
				)
			},
		},
		{
			name: "test_2",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodGet, "/couriers/1", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/couriers/:courier_id")
				args.ctx.SetParamNames("courier_id")
				args.ctx.SetParamValues("abc")
			},
		},
		{
			name: "test_3",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodGet, "/couriers/1", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/couriers/:courier_id")
				args.ctx.SetParamNames("courier_id")
				args.ctx.SetParamValues("1")

				fields.ctrl.EXPECT().GetCourierById(gomock.Any(), int64(1)).Return(
					model.CourierDto{},
					model.HandlerError{
						Err:  errors.New("err"),
						Type: 500,
					},
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}

			if err := h.GetCourierById(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GetCourierById() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHandler_CreateCourier(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodPost, "/couriers", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				fields.ctrl.EXPECT().CreateCourier(gomock.Any(), model.CreateCourierRequest{}).Return(
					model.CreateCouriersResponse{},
					model.HandlerError{},
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}

			if err := h.CreateCourier(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("CreateCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHandler_GetCourierMetaInfo(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodGet, "/couriers/meta-info/:courier_id", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/couriers/meta-info/:courier_id")
				args.ctx.SetParamNames("courier_id")
				args.ctx.SetParamValues("1")

				//fields.ctrl.EXPECT().GetCourierMetaInfo(gomock.Any(), int64(1), time.Now(), time.Now()).Return(
				//	model.GetCourierMetaInfoResponse{},
				//	model.HandlerError{},
				//)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}

			if err := h.GetCourierMetaInfo(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("CreateCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestHandler_GetCouriers(t *testing.T) {
	type fields struct {
		ctrl *mock.MockController
	}

	type args struct {
		ctx echo.Context
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{ctx: echo.New().NewContext(
				httptest.NewRequest(http.MethodGet, "/couriers", nil),
				httptest.NewRecorder(),
			)},
			wantErr: false,
			prepare: func(args args, fields fields) {
				args.ctx.SetPath("/couriers")

				//fields.ctrl.EXPECT().GetCourierMetaInfo(gomock.Any(), int64(1), time.Now(), time.Now()).Return(
				//	model.GetCourierMetaInfoResponse{},
				//	model.HandlerError{},
				//)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				ctrl: mock.NewMockController(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			h := &Handler{
				ctrl: fields.ctrl,
			}

			if err := h.GetCouriers(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("CreateCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
