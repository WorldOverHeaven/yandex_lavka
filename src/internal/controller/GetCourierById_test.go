package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_GetCourierById(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx context.Context
		id  int64
	}
	tests := []struct {
		name    string
		args    args
		want    model.CourierDto
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: model.CourierDto{
				CourierId:    1,
				CourierType:  "FOOT",
				Regions:      []int32{1},
				WorkingHours: []string{"00:00-23:59"},
			},
			want1: model.HandlerError{
				Err:  nil,
				Type: 0,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierById(gomock.Any(), args.id).Return(
					db_model.Courier{
						ID:          1,
						CourierType: "FOOT",
						Region:      []int32{1},
						WorkingHour: []string{"00:00-23:59"},
					},
					nil,
				)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: model.CourierDto{},
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierById(gomock.Any(), args.id).Return(
					db_model.Courier{},
					errors.New("err"),
				)
			},
		},
		{
			name: "test_3",
			args: args{
				ctx: context.Background(),
				id:  -1,
			},
			want: model.CourierDto{},
			want1: model.HandlerError{
				Err:  errors.New("incorrect id"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}
			got, got1 := c.GetCourierById(tt.args.ctx, tt.args.id)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourierById() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				if !(got1.Err.Error() == tt.want1.Err.Error()) {
					t.Errorf("GetCourierById() got1 = %v, want %v", got1, tt.want1)
				}
			}
		})
	}
}
