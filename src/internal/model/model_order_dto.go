/*
 * Yandex Lavka
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 1.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package model

import (
	"time"
)

type OrderDto struct {
	OrderId int64 `json:"order_id"`

	Weight float32 `json:"weight"`

	Regions int32 `json:"regions"`

	DeliveryHours []string `json:"delivery_hours"`

	Cost int32 `json:"cost"`

	CompletedTime time.Time `json:"completed_time,omitempty"`
}
