package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"time"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_GetCourierMetaInfo(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx       context.Context
		courierId int64
		startDate time.Time
		endDate   time.Time
	}
	tests := []struct {
		name    string
		args    args
		want    model.GetCourierMetaInfoResponse
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx:       context.Background(),
				courierId: 1,
				startDate: time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{
				CourierId:    1,
				CourierType:  "FOOT",
				Regions:      []int32{1},
				WorkingHours: []string{"11:00-15:00"},
				Rating:       0,
				Earnings:     6000,
			},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierMetaInfo(gomock.Any(), args.courierId, args.startDate, args.endDate).Return(
					[]db_model.Order{
						{
							ID:            1,
							Weight:        1,
							Region:        1,
							Cost:          1000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
						{
							ID:            2,
							Weight:        1,
							Region:        1,
							Cost:          2000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
					},
					nil,
				)
				fields.storage.EXPECT().GetCourierById(gomock.Any(), args.courierId).Return(
					db_model.Courier{
						ID:          1,
						CourierType: "FOOT",
						Region:      []int32{1},
						WorkingHour: []string{"11:00-15:00"},
					},
					nil,
				)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx:       context.Background(),
				courierId: -1,
				startDate: time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{},
			want1: model.HandlerError{
				Err:  errors.New("error in courier_id data"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_3",
			args: args{
				ctx:       context.Background(),
				courierId: 1,
				startDate: time.Date(2023, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{},
			want1: model.HandlerError{
				Err:  errors.New("incorrect dates"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_4",
			args: args{
				ctx:       context.Background(),
				courierId: 1,
				startDate: time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{},
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierMetaInfo(gomock.Any(), args.courierId, args.startDate, args.endDate).Return(
					nil,
					errors.New("err"),
				)
			},
		},
		{
			name: "test_5",
			args: args{
				ctx:       context.Background(),
				courierId: 1,
				startDate: time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{},
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierMetaInfo(gomock.Any(), args.courierId, args.startDate, args.endDate).Return(
					[]db_model.Order{
						{
							ID:            1,
							Weight:        1,
							Region:        1,
							Cost:          1000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
						{
							ID:            2,
							Weight:        1,
							Region:        1,
							Cost:          2000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
					},
					nil,
				)
				fields.storage.EXPECT().GetCourierById(gomock.Any(), args.courierId).Return(
					db_model.Courier{},
					errors.New("err"),
				)
			},
		},
		{
			name: "test_6",
			args: args{
				ctx:       context.Background(),
				courierId: 1,
				startDate: time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{
				CourierId:    1,
				CourierType:  "FOOT",
				Regions:      []int32{1},
				WorkingHours: []string{"11:00-15:00"},
				Rating:       0,
				Earnings:     0,
			},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierMetaInfo(gomock.Any(), args.courierId, args.startDate, args.endDate).Return(
					[]db_model.Order{},
					nil,
				)
				fields.storage.EXPECT().GetCourierById(gomock.Any(), args.courierId).Return(
					db_model.Courier{
						ID:          1,
						CourierType: "FOOT",
						Region:      []int32{1},
						WorkingHour: []string{"11:00-15:00"},
					},
					nil,
				)
			},
		},
		{
			name: "test_7",
			args: args{
				ctx:       context.Background(),
				courierId: 1,
				startDate: time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{
				CourierId:    1,
				CourierType:  "BIKE",
				Regions:      []int32{1},
				WorkingHours: []string{"11:00-15:00"},
				Rating:       0,
				Earnings:     9000,
			},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierMetaInfo(gomock.Any(), args.courierId, args.startDate, args.endDate).Return(
					[]db_model.Order{
						{
							ID:            1,
							Weight:        1,
							Region:        1,
							Cost:          1000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
						{
							ID:            2,
							Weight:        1,
							Region:        1,
							Cost:          2000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
					},
					nil,
				)
				fields.storage.EXPECT().GetCourierById(gomock.Any(), args.courierId).Return(
					db_model.Courier{
						ID:          1,
						CourierType: "BIKE",
						Region:      []int32{1},
						WorkingHour: []string{"11:00-15:00"},
					},
					nil,
				)
			},
		},
		{
			name: "test_8",
			args: args{
				ctx:       context.Background(),
				courierId: 1,
				startDate: time.Date(2021, 1, 1, 0, 0, 0, 0, time.Local),
				endDate:   time.Date(2021, 1, 10, 0, 0, 0, 0, time.Local),
			},
			want: model.GetCourierMetaInfoResponse{
				CourierId:    1,
				CourierType:  "AUTO",
				Regions:      []int32{1},
				WorkingHours: []string{"11:00-15:00"},
				Rating:       0,
				Earnings:     12000,
			},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetCourierMetaInfo(gomock.Any(), args.courierId, args.startDate, args.endDate).Return(
					[]db_model.Order{
						{
							ID:            1,
							Weight:        1,
							Region:        1,
							Cost:          1000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
						{
							ID:            2,
							Weight:        1,
							Region:        1,
							Cost:          2000,
							CompletedTime: time.Time{},
							DeliveryHour:  nil,
						},
					},
					nil,
				)
				fields.storage.EXPECT().GetCourierById(gomock.Any(), args.courierId).Return(
					db_model.Courier{
						ID:          1,
						CourierType: "AUTO",
						Region:      []int32{1},
						WorkingHour: []string{"11:00-15:00"},
					},
					nil,
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}
			got, got1 := c.GetCourierMetaInfo(tt.args.ctx, tt.args.courierId, tt.args.startDate, tt.args.endDate)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourierMetaInfo() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				if !(got1.Err.Error() == tt.want1.Err.Error()) {
					t.Errorf("GetCourierById() got1 = %v, want %v", got1, tt.want1)
				}
			}
		})
	}
}
