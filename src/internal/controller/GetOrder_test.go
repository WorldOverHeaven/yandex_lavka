package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"time"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_GetOrder(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx context.Context
		id  int64
	}
	tests := []struct {
		name    string
		args    args
		want    model.OrderDto
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: model.OrderDto{
				OrderId:       1,
				Weight:        2,
				Regions:       3,
				DeliveryHours: []string{"00:00-11:11"},
				Cost:          100,
				CompletedTime: time.Time{},
			},
			want1: model.HandlerError{
				Err:  nil,
				Type: 0,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetOrder(gomock.Any(), args.id).Return(
					db_model.Order{
						ID:            1,
						Weight:        2,
						Region:        3,
						Cost:          100,
						CompletedTime: time.Time{},
						DeliveryHour:  []string{"00:00-11:11"},
					},
					nil,
				)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx: context.Background(),
				id:  -1,
			},
			want: model.OrderDto{},
			want1: model.HandlerError{
				Err:  errors.New("incorrect id"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_3",
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: model.OrderDto{},
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().GetOrder(gomock.Any(), args.id).Return(
					db_model.Order{},
					errors.New("err"),
				)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}
			got, got1 := c.GetOrder(tt.args.ctx, tt.args.id)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetOrder() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetOrder() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
