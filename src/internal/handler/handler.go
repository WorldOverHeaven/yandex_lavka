package handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"yandex_lavka/internal/model"
)

var baseBadResponse = model.BadRequestResponse{}

// Handler struct for declaring api methods
type Handler struct {
	ctrl Controller
}

// NewHandler constructor for Handler, user for code generation in wire
func NewHandler(ctrl Controller) *Handler {
	return &Handler{ctrl: ctrl}
}

// Close closes db connection
func (h *Handler) Close() {
}

// e.GET("/ping", ping)
func (h *Handler) ping(ctx echo.Context) error {
	return ctx.String(http.StatusOK, "pong")
}

// Index e.GET("/", Index)
func (h *Handler) Index(ctx echo.Context) error {
	return ctx.String(http.StatusOK, "Hello world!")
}
