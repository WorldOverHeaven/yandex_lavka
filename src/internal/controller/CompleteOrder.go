package controller

import (
	"context"
	"yandex_lavka/internal/model"
)

// CompleteOrder e.POST("/orders/complete", CompleteOrder)
func (c *Controller) CompleteOrder(ctx context.Context, dto model.CompleteOrderRequestDto) ([]model.OrderDto, model.HandlerError) {
	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	orders, err := c.storage.CompleteOrder(ctx, dto.CompleteInfo)
	if err != nil {
		return nil, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	returnOrders := make([]model.OrderDto, 0, len(orders))
	for _, val := range orders {
		returnOrders = append(returnOrders, model.OrderDto{
			OrderId:       val.ID,
			Weight:        val.Weight,
			Regions:       val.Region,
			DeliveryHours: val.DeliveryHour,
			Cost:          val.Cost,
			CompletedTime: val.CompletedTime,
		})
	}

	return returnOrders, model.HandlerError{}
}
