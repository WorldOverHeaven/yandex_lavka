package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"time"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_CompleteOrder(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx context.Context
		dto model.CompleteOrderRequestDto
	}

	tests := []struct {
		name    string
		args    args
		want    []model.OrderDto
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx: context.Background(),
				dto: model.CompleteOrderRequestDto{
					CompleteInfo: []model.CompleteOrder{
						{
							CourierId:    1,
							OrderId:      1,
							CompleteTime: time.Time{},
						},
					},
				},
			},
			want: []model.OrderDto{
				{
					OrderId:       1,
					Weight:        1,
					Regions:       1,
					DeliveryHours: []string{"11:00-12:00"},
					Cost:          1,
					CompletedTime: time.Time{},
				},
			},
			want1: model.HandlerError{
				Err:  nil,
				Type: 0,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().CompleteOrder(gomock.Any(), args.dto.CompleteInfo).Return(
					[]db_model.Order{
						{
							ID:            1,
							Weight:        1,
							Region:        1,
							Cost:          1,
							CompletedTime: time.Time{},
							DeliveryHour:  []string{"11:00-12:00"},
						},
					},
					nil)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx: context.Background(),
				dto: model.CompleteOrderRequestDto{
					CompleteInfo: []model.CompleteOrder{
						{
							CourierId:    1,
							OrderId:      1,
							CompleteTime: time.Time{},
						},
					},
				},
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().CompleteOrder(gomock.Any(), args.dto.CompleteInfo).Return(
					nil,
					errors.New("err"))
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}

			got, got1 := c.CompleteOrder(tt.args.ctx, tt.args.dto)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CompleteOrder() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("CompleteOrder() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
