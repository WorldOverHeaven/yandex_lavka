package controller

import (
	"context"
	"github.com/pkg/errors"
	"time"
	"yandex_lavka/internal/model"
)

// GetCourierMetaInfo e.GET("/couriers/meta-info/:courier_id", GetCourierMetaInfo)
func (c *Controller) GetCourierMetaInfo(ctx context.Context, courierId int64, startDate, endDate time.Time) (model.GetCourierMetaInfoResponse, model.HandlerError) {
	if endDate.Sub(startDate).Hours() <= 0 {
		return model.GetCourierMetaInfoResponse{}, model.HandlerError{
			Err:  errors.New("incorrect dates"),
			Type: 500,
		}
	}

	if courierId <= 0 {
		return model.GetCourierMetaInfoResponse{}, model.HandlerError{
			Err:  errors.New("error in courier_id data"),
			Type: 500,
		}
	}

	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	orders, err := c.storage.GetCourierMetaInfo(ctx, courierId, startDate, endDate)
	if err != nil {
		return model.GetCourierMetaInfoResponse{}, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	courier, err := c.storage.GetCourierById(ctx, courierId)
	if err != nil {
		return model.GetCourierMetaInfoResponse{}, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	responseModel := model.GetCourierMetaInfoResponse{
		CourierId:    courier.ID,
		CourierType:  courier.CourierType,
		Regions:      courier.Region,
		WorkingHours: courier.WorkingHour,
	}

	if len(orders) == 0 {
		return responseModel, model.HandlerError{}
	}

	var cRating, cEarning int32

	switch courier.CourierType {
	case "FOOT":
		cRating = 3
		cEarning = 2
	case "BIKE":
		cRating = 2
		cEarning = 3
	case "AUTO":
		cRating = 1
		cEarning = 4
	}

	hours := endDate.Sub(startDate).Hours()

	rating := (int32(len(orders)) / int32(hours)) * cRating
	var earnings int32
	earnings = 0
	for _, val := range orders {
		earnings += val.Cost
	}
	earnings = earnings * cEarning

	responseModel.Earnings = earnings
	responseModel.Rating = rating

	return responseModel, model.HandlerError{}
}
