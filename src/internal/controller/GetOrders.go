package controller

import (
	"context"
	"errors"
	"yandex_lavka/internal/model"
)

func (c *Controller) GetOrders(ctx context.Context, limit, offset int32) ([]model.OrderDto, model.HandlerError) {
	if limit < 0 || offset < 0 {
		return nil, model.HandlerError{
			Err:  errors.New("error in limit offset"),
			Type: 500,
		}
	}

	ctx, cancel := context.WithTimeout(ctx, contextTimeout)
	defer cancel()

	orders, err := c.storage.GetOrders(ctx, limit, offset)
	if err != nil {
		return nil, model.HandlerError{
			Err:  err,
			Type: 500,
		}
	}

	returnOrders := make([]model.OrderDto, len(orders))
	for key, val := range orders {
		returnOrders[key] = model.OrderDto{
			OrderId:       val.ID,
			Weight:        val.Weight,
			Regions:       val.Region,
			DeliveryHours: val.DeliveryHour,
			Cost:          val.Cost,
			CompletedTime: val.CompletedTime,
		}
	}

	return returnOrders, model.HandlerError{}
}
