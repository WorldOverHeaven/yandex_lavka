package handler

import (
	"github.com/labstack/echo/v4"
)

func SetupRoutes(e *echo.Echo, handler *Handler) {
	e.GET("/ping", handler.ping)

	e.GET("/", handler.Index)

	e.GET("/couriers/assignments", handler.CouriersAssignments)

	e.POST("/couriers", handler.CreateCourier)

	e.GET("/couriers/:courier_id", handler.GetCourierById)

	e.GET("/couriers/meta-info/:courier_id", handler.GetCourierMetaInfo)

	e.GET("/couriers", handler.GetCouriers)

	e.POST("/orders/complete", handler.CompleteOrder)

	e.POST("/orders", handler.CreateOrder)

	e.POST("/orders/assign", handler.OrdersAssign)

	e.GET("/orders/:order_id", handler.GetOrder)

	e.GET("/orders", handler.GetOrders)
}
