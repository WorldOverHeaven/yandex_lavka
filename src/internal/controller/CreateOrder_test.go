package controller

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"reflect"
	"testing"
	"time"
	"yandex_lavka/internal/mock"
	"yandex_lavka/internal/model"
	"yandex_lavka/internal/storage/db_model"
)

func TestController_CreateOrder(t *testing.T) {
	type fields struct {
		storage *mock.MockStorage
	}

	type args struct {
		ctx     context.Context
		request model.CreateOrderRequest
	}
	tests := []struct {
		name    string
		args    args
		want    []model.OrderDto
		want1   model.HandlerError
		prepare func(args, fields)
	}{
		{
			name: "test_1",
			args: args{
				ctx: context.Background(),
				request: model.CreateOrderRequest{Orders: []model.CreateOrderDto{model.CreateOrderDto{
					Weight:        1,
					Regions:       2,
					DeliveryHours: []string{"11:00-12:00"},
					Cost:          3,
				}}},
			},
			want: []model.OrderDto{model.OrderDto{
				OrderId:       1,
				Weight:        1,
				Regions:       2,
				DeliveryHours: []string{"11:00-12:00"},
				Cost:          3,
				CompletedTime: time.Time{},
			}},
			want1: model.HandlerError{},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().CreateOrders(gomock.Any(), []db_model.Order{db_model.Order{
					ID:            0,
					Weight:        1,
					Region:        2,
					Cost:          3,
					CompletedTime: time.Time{},
					DeliveryHour:  []string{"11:00-12:00"},
				}}).Return(
					[]db_model.Order{db_model.Order{
						ID:            1,
						Weight:        1,
						Region:        2,
						Cost:          3,
						CompletedTime: time.Time{},
						DeliveryHour:  []string{"11:00-12:00"},
					}},
					nil,
				)
			},
		},
		{
			name: "test_2",
			args: args{
				ctx: context.Background(),
				request: model.CreateOrderRequest{Orders: []model.CreateOrderDto{model.CreateOrderDto{
					Weight:        1,
					Regions:       2,
					DeliveryHours: []string{"11:00-12:00"},
					Cost:          3,
				}}},
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("err"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
				fields.storage.EXPECT().CreateOrders(gomock.Any(), []db_model.Order{db_model.Order{
					ID:            0,
					Weight:        1,
					Region:        2,
					Cost:          3,
					CompletedTime: time.Time{},
					DeliveryHour:  []string{"11:00-12:00"},
				}}).Return(
					nil,
					errors.New("err"),
				)
			},
		},
		{
			name: "test_3",
			args: args{
				ctx: context.Background(),
				request: model.CreateOrderRequest{Orders: []model.CreateOrderDto{model.CreateOrderDto{
					Weight:        -10,
					Regions:       2,
					DeliveryHours: []string{"11:00-12:00"},
					Cost:          3,
				}}},
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("error in weight data"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_4",
			args: args{
				ctx: context.Background(),
				request: model.CreateOrderRequest{Orders: []model.CreateOrderDto{model.CreateOrderDto{
					Weight:        1,
					Regions:       -2,
					DeliveryHours: []string{"11:00-12:00"},
					Cost:          3,
				}}},
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("error in region data"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_5",
			args: args{
				ctx: context.Background(),
				request: model.CreateOrderRequest{Orders: []model.CreateOrderDto{model.CreateOrderDto{
					Weight:        10,
					Regions:       2,
					DeliveryHours: []string{"00:00-23:59"},
					Cost:          -993,
				}}},
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("error in cost data"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		{
			name: "test_6",
			args: args{
				ctx: context.Background(),
				request: model.CreateOrderRequest{Orders: []model.CreateOrderDto{model.CreateOrderDto{
					Weight:        10,
					Regions:       2,
					DeliveryHours: []string{"0"},
					Cost:          3,
				}}},
			},
			want: nil,
			want1: model.HandlerError{
				Err:  errors.New("error in hours"),
				Type: 500,
			},
			prepare: func(args args, fields fields) {
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)

			fields := fields{
				storage: mock.NewMockStorage(mockCtrl),
			}

			tt.prepare(tt.args, fields)

			c := &Controller{
				storage: fields.storage,
			}
			got, got1 := c.CreateOrder(tt.args.ctx, tt.args.request)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateOrder() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("CreateOrder() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
